//Julien Chapel. University of Vilnius, Erasmus.
//Test 1. Initial fact in the right hand side

//1) Rules
Y D Z
X B E Y
A X
C L
L M N

//2) Facts
A B C D E      // test

//3) Goal
Z           // test
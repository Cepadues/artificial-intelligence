//Julien Chapel. University of Vilnius, Erasmus.
//Test 1. Initial fact in the right hand side

//1) Rules
D Z
C D
B C
A B
A G
G Z

//2) Facts
A

//3) Goal
Z           // test
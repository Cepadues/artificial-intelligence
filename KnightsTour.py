import numpy as np

outputFile = "Output/output"
longOutput = False

def isMovable(U, V, Board, N, Long) :
    if( U >= 0 and U < N and V >= 0 and V < N and Board[U, V] == 0):
        if (Long) : print("Free. ", sep='', end='', file=open(outputFile + "-long.txt", 'a'))
        return True
        
    if( U >= 0 and U < N and V >= 0 and V < N and Board[U, V] != 0):
        if (Long) : print("Thread. ", sep='', end='', file=open(outputFile + "-long.txt", 'a'))
        return False

    if (Long) : print("Out. ", sep='', end='', file=open(outputFile + "-long.txt", 'a'))
    return False


def printData(Size, X, Y) : 
    if (longOutput) :  
        print("PART 1. Data", file=open(outputFile + "-long.txt", 'a'))
        print("   ", "1) Board:", Size, "x", Size, file=open(outputFile + "-long.txt", 'a'))
        print("   ", "2) Initial position: X=", X+1, "Y=", Y+1, "L=1.", file=open(outputFile + "-long.txt", 'a'))

    print("PART 1. Data")
    print("   ", "1) Board:", Size, "x", Size)
    print("   ", "2) Initial position: X=", X+1, "Y=", Y+1, "L=1.")

    print("PART 1. Data", file=open(outputFile + "-short.txt", 'a'))
    print("   ", "1) Board:", Size, "x", Size, file=open(outputFile + "-short.txt", 'a'))
    print("   ", "2) Initial position: X=", X+1, "Y=", Y+1, "L=1.", file=open(outputFile + "-short.txt", 'a'))


# Part 3
def printResult(Board, Size) : 
    if (longOutput) :
        print("\nPART 3. Results", Trials, file=open(outputFile + "-long.txt", 'a'))
        print("   ", "1) Path is found. Trials=", Trials, file=open(outputFile + "-long.txt", 'a'))
        print("   ", "2) Path graphically:",  file=open(outputFile + "-long.txt", 'a'))
        print( file=open(outputFile + "-long.txt", 'a'))

    print("\nPART 3. Results")
    print("   ", "1) Path is found. Trials=", Trials)
    print("   ", "2) Path graphically:")
    print()

    print("\nPART 3. Results", file=open(outputFile + "-short.txt", 'a'))
    print("   ", "1) Path is found. Trials=", Trials, file=open(outputFile + "-short.txt", 'a'))
    print("   ", "2) Path graphically:", file=open(outputFile + "-short.txt", 'a'))
    print(file=open(outputFile + "-short.txt", 'a'))

    print("Y, V ^")
    for i in range(Size):
        print("  ", Size-i, "|", end=' ')
        for j in range(Size):
            print("%2d" % Board[j][Size-i-1], end=' ')
        print()

    print("     ", end='')
    for i in range(Size):
        print("---", end='')
    print("--> X, U")

    print("        ", end='')
    for i in range(Size):
        print(i+1, end='  ')
    print()

    print("Y, V ^", file=open(outputFile + "-short.txt", 'a'))
    for i in range(Size):
        print("  ", Size-i, "|", end=' ', file=open(outputFile + "-short.txt", 'a'))
        for j in range(Size):
            print("%2d" % Board[j][Size-i-1], end=' ', file=open(outputFile + "-short.txt", 'a'))
        print(file=open(outputFile + "-short.txt", 'a'))

    print("     ", end='', file=open(outputFile + "-short.txt", 'a'))
    for i in range(Size):
        print("---", end='', file=open(outputFile + "-short.txt", 'a'))
    print("--> X, U", file=open(outputFile + "-short.txt", 'a'))

    print("        ", end='', file=open(outputFile + "-short.txt", 'a'))
    for i in range(Size):
        print(i+1, end='  ', file=open(outputFile + "-short.txt", 'a'))
    print(file=open(outputFile + "-short.txt", 'a'))

    if (longOutput):
        print("Y, V ^", file=open(outputFile + "-long.txt", 'a'))
        for i in range(Size):
            print("  ", Size-i, "|", end=' ', file=open(outputFile + "-long.txt", 'a'))
            for j in range(Size):
                print("%2d" % Board[j][Size-i-1], end=' ', file=open(outputFile + "-long.txt", 'a'))
            print(file=open(outputFile + "-long.txt", 'a'))

        print("     ", end='', file=open(outputFile + "-long.txt", 'a'))
        for i in range(Size):
            print("---", end='', file=open(outputFile + "-long.txt", 'a'))
        print("--> X, U", file=open(outputFile + "-long.txt", 'a'))

        print("        ", end='', file=open(outputFile + "-long.txt", 'a'))
        for i in range(Size):
            print(i+1, end='  ', file=open(outputFile + "-long.txt", 'a'))
        print(file=open(outputFile + "-long.txt", 'a'))

CX = [2, 1, -1, -2, -2, -1, 1, 2]   # Production Set
CY = [1, 2, 2, 1, -1, -2, -2, -1]   # Possible movements of the kinght

Trials = 0

def Solver(Board, N, L, X, Y, Long):
    global CX, CY, Trials
    
    # print('{0: >{width}}'.format(Moves, width =  width) , '.', ' Move disk ', N, sep='', end='')
    
    if (L == N * N + 1) :
        return True
    
    for k in range(8) :
        U = X + CX[k]
        V = Y + CY[k]
        Trials = Trials + 1

        if (Long) : print('\n', '{0: >8}'.format(Trials), ") ", "-" * (L - 2), 'R', k + 1, '. ', sep='', end='', file=open(outputFile + "-long.txt", 'a'))
        if (Long) : print('U=%d, V=%d. L=%d. ' % (U + 1, V + 1, L) , sep='', end='', file=open(outputFile + "-long.txt", 'a'))
        
        if (isMovable(U, V, Board, N, Long)) :
            Board[U][V] = L
            if (Long) : print('BOARD[%d,%d]=>%d.' % (U + 1, V + 1, L), sep='', end='', file=open(outputFile + "-long.txt", 'a'))

            if (Solver(Board, N, L + 1, U, V, Long)) :
                return True
            
            Board[U][V] = 0
    return False


def Main(Size, StartX, StartY, Long):

    printData(Size, StartX, StartY)
    Board = np.zeros((Size,Size)) 
    Board[StartX, StartY] = 1
    if (Long) : print("\nPART 2. Trace", end='', file=open(outputFile + "-long.txt", 'a'))
    if(Solver(Board, Size, 2, StartX, StartY, Long)):
        printResult(Board, Size)
    else:
        print("\nPART 3. Results")
        print("   ", "1) No path found. Trials=", Trials)
        print("\nPART 3. Results", file=open(outputFile + "-short.txt", 'a'))
        print("   ", "1) No path found. Trials=", Trials, file=open(outputFile + "-short.txt", 'a'))
        if longOutput:
            print("\nPART 3. Results", file=open(outputFile + "-long.txt", 'a'))
            print("   ", "1) No path found. Trials=", Trials, file=open(outputFile + "-long.txt", 'a'))

def clearFile():
    global longOutput, outputFile
    print(file=open(outputFile + "-short.txt", 'w'), end='')
    if longOutput: print(file=open(outputFile + "-long.txt", 'w'), end='')

def runAllTest():
    global longOutput, Trials, outputFile
    Trials = 0
    print("\n###### Test 1 ######")
    N = 5
    X = 0
    Y = 0
    outputFile = "Output/test1-out"
    clearFile()
    Main(N, X, Y, longOutput)
    Trials = 0
    print("\n###### Test 2 ######")
    N = 5
    X = 4
    Y = 0
    outputFile = "Output/test2-out"
    clearFile()
    Main(N, X, Y, longOutput)
    Trials = 0
    print("\n###### Test 3 ######")
    N = 5
    X = 0
    Y = 4
    outputFile = "Output/test3-out"
    clearFile()
    Main(N, X, Y, longOutput)
    Trials = 0
    print("\n###### Test 4 ######")
    N = 5
    X = 1
    Y = 0
    outputFile = "Output/test4-out"
    clearFile()
    Main(N, X, Y, longOutput)
    Trials = 0
    print("\n###### Test 5 ######")
    N = 6
    X = 0
    Y = 0
    outputFile = "Output/test5-out"
    clearFile()
    Main(N, X, Y, longOutput)
    Trials = 0
    print("\n###### Test 6 ######")
    N = 7
    X = 0
    Y = 0
    outputFile = "Output/test6-out"
    clearFile()
    Main(N, X, Y, longOutput)
    Trials = 0
    print("\n###### Test 7 ######")
    N = 4
    X = 0
    Y = 0
    outputFile = "Output/test7-out"
    clearFile()
    Main(N, X, Y, longOutput)



def mainProgram(choice):
    global longOutput, Trials

    if(choice == '1'):
        print("Would you like to save the trace to a file ? (y/n)")
        mainProgram(input())
        runAllTest()

    elif(choice == '2'):
        print("Would you like to save the trace to a file ? (y/n)")
        mainProgram(input())
        print("Please enter the size of the board N :")
        N = int(input())
        print("Please enter the starting position of the knight X :")
        X = int(input())
        print("Please enter the starting position of the knight Y :")
        Y = int(input())
        clearFile()
        Main(N, X-1, Y-1, longOutput)
        
    elif(choice == 0):
        print("###### Knight's Tour problem solver ######")
        print("1) Run all tests \n2) Run with given parameters \n Your choice : ")
        mainProgram(input())
    elif(choice == 'y'):
        longOutput = True
    elif(choice == 'n'):
        longOutput = False
    else:
        print("Wrong input")
        mainProgram(0)

mainProgram(0)